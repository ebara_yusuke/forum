package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.repository.DateRepository;
import com.example.demo.repository.ReportRepository;

@Service
@Transactional
public class ReportService {

	@Autowired
	ReportRepository reportRepository;

	@Autowired
	DateRepository dateRepository;

	//レコード全件取得
	public List<Report> findAllReport() {
		return reportRepository.findAllByOrderByUpdatedDateDesc();
	}
	//レコード追加
	public void saveReport(Report report) {
		report.setUpdatedDate(new Date());
		reportRepository.save(report);
	}
	//レコード削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}
	//レコードを1件取得
	public Report editReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}
	//レコードを指定した期間取得
	public List<Report> findReport(String start_date, String end_date) throws ParseException {

		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		String start = null;
		String end = null;
		String nowDate = sdFormat.format(date);

		if(start_date != "") {
			start = start_date + " 00:00:00";
		} else {
			start = "2020-01-01 00:00:00";
		}

		if(end_date != "") {
			end = end_date + " 23:59:59";
		} else {
			end = nowDate;
		}

		Date startDate = sdFormat.parse(start);
		Date endDate = sdFormat.parse(end);

		List<Report> report = dateRepository.findByCreatedDateBetween(startDate, endDate);
		return report;
	}
	//レコードに投稿IDと更新日時を引き渡す
	public void updatedDate (Comment comment) {
		Report report = new Report();
		comment.setUpdatedDate(new Date());
		report.setId(comment.getReport_id());
		report.setUpdatedDate(comment.getUpdatedDate());
		reportRepository.saveUpdatedDate(report.getId() , report.getUpdatedDate());
	}
}

package com.example.demo.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;
import com.example.demo.repository.ReportRepository;

@Service
@Transactional
public class CommentService {

	@Autowired
	CommentRepository commentRepository;

	@Autowired
	ReportRepository reportRepository;

	//全件コメント取得
	public List<Comment> findAllComment(){
		return commentRepository.findAllByOrderByUpdatedDateDesc();
	}
	//コメント追加
	public void saveComment(Comment content) {
		content.setUpdatedDate(new Date());
		commentRepository.save(content);
	}
	//コメント削除
	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}
	//コメントを１件取得
	public Comment editComment(Integer id) {
		Comment comment = (Comment) commentRepository.findById(id).orElse(null);
		return comment;
	}
}

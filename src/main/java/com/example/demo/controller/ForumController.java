package com.example.demo.controller;



import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {

	@Autowired
	ReportService reportService;

	@Autowired
	CommentService commentService;

	// 投稿内容表示画面
	@GetMapping({"/", "/top"})
	public ModelAndView top() {
		ModelAndView mav = new ModelAndView();
		// 投稿を全件取得
		List<Report> contentData = reportService.findAllReport();
		List<Comment> commentContentData = commentService.findAllComment();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		mav.addObject("commentContents", commentContentData);
		return mav;
	}

	@GetMapping("/date")
	public ModelAndView topDate(@RequestParam(name="start") String start_date, @RequestParam(name="end") String end_date) throws ParseException{
		ModelAndView mav = new ModelAndView();

		List<Report> contentData = reportService.findReport(start_date , end_date);
		List<Comment> commentContentData = commentService.findAllComment();

		mav.setViewName("/top");

		mav.addObject("contents", contentData);
		mav.addObject("commentContents", commentContentData);

		return mav;
	}

	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		//form用の空のentityを準備
		Report report = new Report();
		//画面遷移先を指定
		mav.setViewName("/new");
		//準備した空のentityを保管
		mav.addObject("formModel",report);
		return mav;
	}

	//投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		//投稿をテーブルに格納
		reportService.saveReport(report);
		//rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		reportService.deleteReport(id); //UrlParameterのidを元に投稿を削除
		return new ModelAndView("redirect:/");//rootにリダイレクト
	}

	//編集画面
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		//編集する投稿を取得
		Report report = reportService.editReport(id);
		//編集する投稿をセット
		mav.addObject("formModel", report);
		//画面遷移先を指定
		mav.setViewName("/edit");
		return mav;
	}

	//編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent (@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
		//UrlParameterのidを更新するentityにセット
		report.setId(id);
		//編集した投稿を更新
		reportService.saveReport(report);
		//rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//コメント投稿処理
	@PostMapping("/add_comment")
	public ModelAndView addCommentContent(@ModelAttribute("formModel") Comment comment) {
		//投稿をテーブルに格納
		commentService.saveComment(comment);
		reportService.updatedDate(comment);
		//rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//コメント編集画面
	@GetMapping("/edit_comment/{id}")
	public ModelAndView editCommentContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		//編集する投稿を取得
		Comment comment = commentService.editComment(id);
		//編集する投稿をセット
		mav.addObject("formModel", comment);
		//画面遷移先を指定
		mav.setViewName("/edit_comment");
		return mav;
	}

	//コメント編集処理
	@PutMapping("/update_comment/{id}")
	public ModelAndView updateCommentContent (@PathVariable Integer id, @ModelAttribute("formModel") Comment comment) {
		//UrlParameterのidを更新するentityにセット
		comment.setId(id);
		//編集したコメントを更新
		commentService.saveComment(comment);
		//rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//コメントの削除処理
	@DeleteMapping("/delete_comment/{id}")
	public ModelAndView deleteCommentContent(@PathVariable Integer id) {
		commentService.deleteComment(id); //UrlParameterのidを元に投稿を削除
		return new ModelAndView("redirect:/");//rootにリダイレクト
	}
}